Feature: List Level 3 Customer Segments
  Lists all level 3 customer segments

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listLevel3CustomerSegments
    Then all level 3 customer segments in the customer-segment-service are returned