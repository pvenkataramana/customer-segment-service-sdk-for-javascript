import Level2CustomerSegmentView from '../src/level2CustomerSegmentView';
import Level1CustomerSegmentView from '../src/level1CustomerSegmentView';

const dummy = {
    firstName: 'firstName',
    lastName: 'lastName',
    customerSegmentId: 1,
    customerSegmentName: 'customerSegmentName',
    accountId: '000000000000000000',
    sapVendorNumber: 'sapVendorNo',
    partnerRepId: 1,
    emailAddress: 'email@test.com',
    url: 'https://dummy-url.com'
};

dummy.level1CustomerSegmentView =
    new Level1CustomerSegmentView(dummy.customerSegmentId, dummy.customerSegmentName);

dummy.level2CustomerSegmentView =
    new Level2CustomerSegmentView(
        dummy.customerSegmentId,
        dummy.customerSegmentName,
        dummy.level1CustomerSegmentView
    );

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default dummy;