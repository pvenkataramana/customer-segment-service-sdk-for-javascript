import Level1CustomerSegmentView from '../../src/level1CustomerSegmentView';
import dummy from '../dummy';

describe('Level1CustomerSegmentView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor = () => new Level1CustomerSegmentView(null, dummy.customerSegmentName);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.customerSegmentId;

            /*
             act
             */
            const objectUnderTest = new Level1CustomerSegmentView(expectedId, dummy.customerSegmentName);

            /*
            assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor = () => new Level1CustomerSegmentView(dummy.customerSegmentId, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.customerSegmentName;

            /*
             act
             */
            const objectUnderTest = new Level1CustomerSegmentView(dummy.customerSegmentId, expectedName);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
    })
});
