import Level3CustomerSegmentView from '../../src/level3CustomerSegmentView';
import dummy from '../dummy';

describe('Level3CustomerSegmentView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level3CustomerSegmentView(null, dummy.customerSegmentName, dummy.level2CustomerSegmentView);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.customerSegmentId;

            /*
             act
             */
            const objectUnderTest =
                new Level3CustomerSegmentView(expectedId, dummy.customerSegmentName,dummy.level2CustomerSegmentView);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level3CustomerSegmentView(dummy.customerSegmentId, null, dummy.level2CustomerSegmentView);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.customerSegmentName;

            /*
             act
             */
            const objectUnderTest =
                new Level3CustomerSegmentView(dummy.customerSegmentId, expectedName,dummy.level2CustomerSegmentView);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('throws if level2CustomerSegment is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new Level3CustomerSegmentView(dummy.customerSegmentId, dummy.customerSegmentName, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'level2CustomerSegment required');
        });
        it('sets level2CustomerSegment', () => {
            /*
             arrange
             */
            const expectedLevel2CustomerSegment = dummy.level2CustomerSegmentView;

            /*
             act
             */
            const objectUnderTest =
                new Level3CustomerSegmentView(
                    dummy.customerSegmentId,
                    dummy.customerSegmentName,
                    dummy.level2CustomerSegmentView
                );

            /*
             assert
             */
            const actualLevel2CustomerSegment = objectUnderTest.level2CustomerSegment;
            expect(actualLevel2CustomerSegment).toEqual(expectedLevel2CustomerSegment);

        });
    })
});
