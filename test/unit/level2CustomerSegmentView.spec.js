import Level2CustomerSegmentView from '../../src/level2CustomerSegmentView';
import dummy from '../dummy';

describe('Level2CustomerSegmentView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level2CustomerSegmentView(null, dummy.customerSegmentName, dummy.level1CustomerSegmentView);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.customerSegmentId;

            /*
             act
             */
            const objectUnderTest =
                new Level2CustomerSegmentView(expectedId, dummy.customerSegmentName, dummy.level1CustomerSegmentView);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level2CustomerSegmentView(dummy.customerSegmentId, null, dummy.level1CustomerSegmentView);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.customerSegmentName;

            /*
             act
             */
            const objectUnderTest =
                new Level2CustomerSegmentView(dummy.customerSegmentId, expectedName, dummy.level1CustomerSegmentView);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('throws if level1CustomerSegment is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new Level2CustomerSegmentView(dummy.customerSegmentId, dummy.customerSegmentName, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'level1CustomerSegment required');
        });
        it('sets level1CustomerSegment', () => {
            /*
             arrange
             */
            const expectedLevel1CustomerSegment = dummy.level1CustomerSegmentView;

            /*
             act
             */
            const objectUnderTest =
                new Level2CustomerSegmentView(
                    dummy.customerSegmentId,
                    dummy.customerSegmentName,
                    dummy.level1CustomerSegmentView
                );

            /*
             assert
             */
            const actualLevel1CustomerSegment = objectUnderTest.level1CustomerSegment;
            expect(actualLevel1CustomerSegment).toEqual(expectedLevel1CustomerSegment);

        });
    })
});
