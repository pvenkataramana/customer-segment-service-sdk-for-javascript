import {inject} from 'aurelia-dependency-injection';
import CustomerSegmentServiceSdkConfig from './customerSegmentServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import Level3CustomerSegmentView from './level3CustomerSegmentView';
import Level3CustomerSegmentViewFactory from './level3CustomerSegmentViewFactory';

@inject(CustomerSegmentServiceSdkConfig, HttpClient)
class ListLevel3CustomerSegmentsFeature {

    _config:CustomerSegmentServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedLevel3CustomerSegments:Array<Level3CustomerSegmentView>;

    constructor(config:CustomerSegmentServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all level3 customer segments
     * @returns {Promise.<Level3CustomerSegmentView[]>}
     */
    execute(accessToken:string):Promise<Array> {

        if (this._cachedLevel3CustomerSegments) {

            return Promise.resolve(this._cachedLevel3CustomerSegments);

        }
        else {
            return this._httpClient
                .createRequest('level-3-customer-segments')
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                        // cache
                        this._cachedLevel3CustomerSegments = Array.from(
                            response.content,
                            (contentItem) =>
                                Level3CustomerSegmentViewFactory.construct(contentItem)
                        );

                        return this._cachedLevel3CustomerSegments;

                    }
                );
        }
    }
}

export default ListLevel3CustomerSegmentsFeature;
