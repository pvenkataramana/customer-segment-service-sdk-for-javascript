import Level2CustomerSegmentViewFactory from './level2CustomerSegmentViewFactory';
import Level3CustomerSegmentView from './level3CustomerSegmentView';

export default class Level3CustomerSegmentViewFactory {

    static construct(data):Level3CustomerSegmentView {

        const level2CustomerSegment =
            Level2CustomerSegmentViewFactory
                .construct(data.level2CustomerSegment);

        return new Level3CustomerSegmentView(
            data.id,
            data.name,
            level2CustomerSegment
        );

    }

}